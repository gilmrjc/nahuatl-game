module.exports = {
  publicPath: process.env.PUBLIC_PATH ? process.env.PUBLIC_PATH : '/',
  lintOnSave: 'default',
  pluginOptions: {
    lintStyleOnBuild: true,
    stylelint: {
      cache: true
    }
  }
}
