module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: [
    'plugin:vue/recommended',
    'airbnb-base',
    '@vue/standard'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  settings: {
    'import/resolver': {
      webpack: {
        config: require.resolve('@vue/cli-service/webpack.config.js')
      }
    }
  },
  rules: {}
}
