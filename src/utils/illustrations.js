import ilustration1 from '@/components/illustrations/iglesia.vue'
import ilustration2 from '@/components/illustrations/plaza.vue'
import ilustration3 from '@/components/illustrations/piramide.vue'
import ilustration4 from '@/components/illustrations/campo.vue'
import ilustration5 from '@/components/illustrations/atlixco.vue'
import ilustration6 from '@/components/illustrations/hibernadero.vue'

const ilustrations = [
  { name: 'iglesia', image: ilustration1 },
  { name: 'plaza', image: ilustration2 },
  { name: 'piramide', image: ilustration3 },
  { name: 'campo', image: ilustration4 },
  { name: 'atlixco', image: ilustration5 },
  { name: 'hibernadero', image: ilustration6 }
]

export default ilustrations
