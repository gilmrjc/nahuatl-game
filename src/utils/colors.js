import amarillo from '@/assets/sounds/Kostik.wav'
import azul from '@/assets/sounds/Yawtik.wav'
import rojo from '@/assets/sounds/Chichiltik.wav'
import blanco from '@/assets/sounds/Istatik.wav'
import verde from '@/assets/sounds/Xoxoktik.wav'
import verdeFuerte from '@/assets/sounds/Xopiaktik.wav'
import naranja from '@/assets/sounds/Chichilkostik.wav'
import cafe from '@/assets/sounds/Chocolatik.wav'
import gris from '@/assets/sounds/Nextik.wav'
import rosa from '@/assets/sounds/chichilistatik.wav'
import morado from '@/assets/sounds/Xokopaltik.wav'
import negro from '@/assets/sounds/Kapustik.wav'

const colors = [
  { id: 1, name: 'Rosa', traduction: 'Chichilistatik', code: '#E817BA', sound: rosa },
  { id: 2, name: 'Cafe', traduction: 'Chocholatik', code: '#936B64', sound: cafe },
  { id: 3, name: 'Verde fuerte', traduction: 'Xopiaktik', code: '#066D29', sound: verdeFuerte },
  { id: 4, name: 'Azul', traduction: 'Yawtik', code: '#07C0FB', sound: azul },
  { id: 5, name: 'Gris', traduction: 'Nextik', code: '#B9B9B9', sound: gris },
  { id: 6, name: 'Morado', traduction: 'Xokopaltik', code: '#9E07FB', sound: morado },
  { id: 7, name: 'Naranja', traduction: 'Chichilkostik', code: '#FF9900', sound: naranja },
  { id: 8, name: 'Verde', traduction: 'Xoxoktik', code: '#17D056', sound: verde },
  { id: 9, name: 'Rojo', traduction: 'Chichiltik', code: '#EB2222', sound: rojo },
  { id: 10, name: 'Amarillo', traduction: 'Kostik', code: '#FBD207', sound: amarillo },
  { id: 11, name: 'Negro', traduction: 'Kapustik', code: '#000', sound: negro },
  { id: 12, name: 'Blanco', traduction: 'Istatik', code: '#FFF', sound: blanco }
]

export default colors
