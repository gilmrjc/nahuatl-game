import juegoColores from '@/assets/sounds/mawiltiani-ika-tlapalmeh.wav'
import pintaNumeros from '@/assets/sounds/tlapalchiwa-ika-tlapowameh.wav'
import juegoNumeros from '@/assets/sounds/mawiltiani-ika-tlapowameh.wav'
import colocaCantidad from '@/assets/sounds/xiktlali-tlapowani.wav'
import juegoCuerpo from '@/assets/sounds/mawiltiani-ika-nakayotl.wav'
import rompecabezasCuerpo from '@/assets/sounds/nowiyampa-tlen-nakayotl.wav'
import historiasNawatl from '@/assets/sounds/tlanonojsake-nawatl.wav'
import escuchaHistorias from '@/assets/sounds/tlakakilis-tlanonojsake.wav'

const options = [
  {
    index: 0,
    original: 'Juego de los colores',
    traduction: 'Mawiltiani ika tlapalmeh',
    link: 'ColorsRoulette',
    sound: juegoColores
  },
  {
    index: 1,
    original: 'Pinta con números',
    traduction: 'Tlapalchiwa ika tlapowameh',
    link: 'ColorsMenu',
    sound: pintaNumeros
  },
  {
    index: 2,
    original: 'Juego de los números',
    traduction: 'Mawiltiani ika tlapowameh',
    sound: juegoNumeros
  },
  {
    index: 3,
    original: 'Coloca la cantidad',
    traduction: 'Xiktlali trapowani',
    sound: colocaCantidad
  },
  {
    index: 4,
    original: 'Juego de las partes del cuerpo',
    traduction: 'Mawiltiani ika nakayotl',
    sound: juegoCuerpo
  },
  {
    index: 5,
    original: 'Rompecabezas de las parted del cuerpo',
    traduction: 'Nowiyampa tlen nakayotl',
    sound: rompecabezasCuerpo
  },
  {
    index: 6,
    original: 'Historias en Nawatl',
    traduction: 'Tlanonojsake nawatl',
    sound: historiasNawatl
  },
  {
    index: 7,
    original: 'Escucha las historias',
    traduction: 'Tlakakilis tlanonojsake',
    sound: escuchaHistorias
  }
]

export default options
