import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/menu',
    name: 'Menu',
    component: () => import(/* webpackChunkName: "menu" */ '../views/Menu.vue')
  },
  {
    path: '/colores/menu',
    name: 'ColorsMenu',
    component: () => import(/* webpackChunkName: "colorsMenu" */ '../views/ColorsMenu.vue')
  },
  {
    path: '/colores/dibujo/:name',
    name: 'ColorsDraw',
    component: () => import(/* webpackChunkName: "colorsMenu" */ '../views/ColorsDraw.vue')
  },
  {
    path: '/colores/ruleta',
    name: 'ColorsRoulette',
    component: () => import(/* webpackChunkName: "colorsRouletter" */ '../views/ColorsRoulette.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
